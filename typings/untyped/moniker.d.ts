// tslint:disable

declare module 'moniker' {

    namespace Moniker {
        export interface IGeneratorOptions {
            maxSize?: number,
            encoding?: 'utf-8',
            glue?: string,
        }

        export interface Generator {
            use(dictionary: Dictionary, options: IDictionaryOptions);
            choose(): string;
        }

        export interface IDictionaryOptions {
            maxSize?: number,
            encoding?: 'utf-8',
        }

        export interface Dictionary {
            read(path: string, options: IDictionaryOptions);
            choose(): string;
        }
    }

    interface Moniker {
        generator(dictionaries: Array<() => Moniker.Dictionary>, options?: Moniker.IGeneratorOptions): Moniker.Generator;
        Generator(options?: Moniker.IGeneratorOptions): Moniker.Generator;
        noun(options?: Moniker.IDictionaryOptions): Moniker.Dictionary;
        verb(options?: Moniker.IDictionaryOptions): Moniker.Dictionary;
        adjective(options?: Moniker.IDictionaryOptions): Moniker.Dictionary;
        Dictionary(): Moniker.Dictionary;
        read(path: string, options: Moniker.IDictionaryOptions): Moniker.Dictionary
    }

    const Moniker: Moniker;

    export = Moniker;
}
