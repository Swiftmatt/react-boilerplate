import logger from 'server/logger';
import setup from 'server/lib/modules/core/middlewares/lib/frontend-middleware';
import { resolve } from 'path';
import { HttpServer, EventName as HttpServerEventName } from 'server/http-server';
import { isDev, NODE_ENV } from 'server/lib/utils/env';
import { defaultLogger } from 'server/lib/logger';
import { Signal } from 'server/lib/enum/signal';
import { createConnection, ConnectionOptions } from 'typeorm';

const argv = require('minimist')(process.argv.slice(2)) as {
    port?: number;
    tunnel?: boolean;
};
interface INgrok {
    connect(port: number, callback: (err: Error, url: string) => void);
}
const ngrok: INgrok = (isDev && process.env.ENABLE_TUNNEL) || argv.tunnel ? require('ngrok') : false;

let httpServerInstance: HttpServer;

async function main() {

    try {
        const ormConfig = require(`${__dirname}/../ormconfig.json`) as ConnectionOptions[];
        await createConnection({
            ...ormConfig.find((x) => x.environment === NODE_ENV),
            logging: {
                logQueries: true,
                logFailedQueryError: true,
            },
            entities: [__dirname + '/lib/modules/**/**/entity/{*.ts,*.js}'],
        });
    } catch (err) {
        defaultLogger.error(`failed to connect to database! ${err}`);
    }

    httpServerInstance = new HttpServer();

    httpServerInstance.register(HttpServerEventName.onPostListen, (app) =>
        // In production we need to pass these values in instead of relying on webpack
        setup(app, {
            outputPath: resolve(process.cwd(), 'deploy'),
            publicPath: '/',
        }),
    );

    // Start your app.
    const nativeHttpServer = await httpServerInstance.start();

    // Connect to ngrok in dev mode
    if (ngrok) {
        ngrok.connect(nativeHttpServer.address().port, (innerErr, url) => {
            if (innerErr) {
                return logger.error(innerErr);
            }

            logger.appStarted(nativeHttpServer.address().port, url);
        });
    } else {
        logger.appStarted(nativeHttpServer.address().port);
    }
}

process.on('unhandledRejection', (reason: Error | any, p: Promise<any>) => {
    defaultLogger.error(`[Process][Unhandled Promise Rejection]: Reason: ${JSON.stringify(reason, null, 2)}`);
    if (reason instanceof Error) {
        throw reason;
    }
    throw new Error(reason);
});

process.on('warning', ({ name, message, stack }) => {
    defaultLogger.warn(`[Process][Warning]: ${name} - ${message}\n ${stack}`);
});

if (process.platform === 'win32') {
    const rl = require('readline').createInterface({
        input: process.stdin,
        output: process.stdout,
    });

    rl.once(Signal.SIGINT, () => {
        process.emit(Signal.SIGINT);
    });
}

const shutdownSignals: Signal[] = [Signal.SIGTERM, Signal.SIGINT];
shutdownSignals.forEach((signal) => process.once(signal, async () => {
    let exitCode = 0;
    try {
        await httpServerInstance.shutdown(signal);
        exitCode = 0;
    } catch (err) {
        exitCode = 1;
    }
    process.exit(exitCode);
}));

main();
