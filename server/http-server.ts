import * as Express from 'express';
import { resolve } from 'path';
import { NestFactory } from '@nestjs/core';
import { CoreModule } from './lib/modules/core';
import * as http from 'http';
import * as BodyParser from 'body-parser';
import Argv from 'server/lib/utils/argv';
import { Logger } from 'server/lib/logger';
import { isDev } from 'server/lib/utils/env';
import { Signal } from 'server/lib/enum/signal';
import * as _ from 'lodash';

import { EnumType, makeEnumFromObject } from 'server/lib/utils/enum';
export type EventName = EnumType<typeof EventName>;
// tslint:disable-next-line:no-var-keyword
export var EventName = makeEnumFromObject({
    onPreConfigure: 'onPreConfigure',
    onPostConfigure: 'onPostConfigure',
    onPostListen: 'onPostListen',
});


export type IExpressInstanceCallback = (expressInstance: Express.Express) => Promise<any> | any;

export interface IConfig {
    port?: number;
}

export interface IEventCallbacks {
    onPreConfigure: IExpressInstanceCallback[];
    onPostConfigure: IExpressInstanceCallback[];
    onPostListen: IExpressInstanceCallback[];
}

export class HttpServer {
    private nativeHttpServer: http.Server;
    private isShuttingDown  = false;
    private config: IConfig = {};

    constructor(private readonly expressInstance?: Express.Express, config?: IConfig) {
        this.expressInstance = expressInstance || Express();
        _.defaults(
            this.config,
            config,
            {
                port: Argv.port || process.env.PORT || 3000,
            },
        );
    }

    public getConfig() {
        return _.clone(this.config);
    }

    public async shutdown(signal: Signal) {
        return new Promise<void>((Ok, Fail) => {
            if (this.isShuttingDown) {
                Fail('already shutting down!');
            }
            this.isShuttingDown = true;
            Logger.info(`received ${signal}, attempting to shutdown gracefully... (killing in 15s)`);
            setTimeout(() => Ok(), 15000);
            try {
                this.nativeHttpServer.close((err) => {
                    if (err) {
                        Logger.error(`Error while shutting down HTTP server: ${err}`);
                    }
                    setTimeout(() => Ok(), isDev ? 0 : 5000);
                });
            } catch (err) {
                Logger.error(err.message);
                Fail(err);
            }
        });
    }

    public async start() {

        this.expressInstance.use(BodyParser.urlencoded({ extended: false }));
        this.expressInstance.use(BodyParser.json());

        // onPreConfigure
        for (const postList of this.callbacks.onPreConfigure) {
            await postList(this.expressInstance);
        }

        const app = NestFactory.create(CoreModule, this.expressInstance);

        // onPostConfigure
        for (const postList of this.callbacks.onPostConfigure) {
            await postList(this.expressInstance);
        }

        // Start your app.
        await new Promise<void>((Ok, Fail) => this.nativeHttpServer = app.listen(this.config.port, async () => {
            Logger.info(`listening on port ${this.nativeHttpServer.address().port}`);
            if (typeof process.send === 'function') {
                Logger.info('broadcasting ready');
                process.send('ready');
            }
            return Ok();
        }));

        // onPostListen
        for (const postList of this.callbacks.onPostListen) {
            await postList(this.expressInstance);
        }

        return this.nativeHttpServer;
    }

    private callbacks: IEventCallbacks = {
        onPreConfigure: [] as IExpressInstanceCallback[],
        onPostConfigure: [] as IExpressInstanceCallback[],
        onPostListen: [] as IExpressInstanceCallback[],
    };
    public register(event: keyof IEventCallbacks | EventName, callback: IExpressInstanceCallback) {
        this.callbacks[event].push(callback);
    }
}

export default HttpServer;
