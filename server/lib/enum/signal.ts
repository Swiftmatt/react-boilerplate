import { EnumType, makeEnumFromObject } from 'server/lib/utils/enum';
export type Signal = EnumType<typeof Signal>;
// tslint:disable-next-line:no-var-keyword
export var Signal = makeEnumFromObject({
    SIGTERM: 'SIGTERM',
    SIGINT: 'SIGINT',
});
