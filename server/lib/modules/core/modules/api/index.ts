import { MiddlewaresConsumer, Module, NestModule } from '@nestjs/common';
import { LoggerService } from 'server/lib/modules/core/components/logger-service';
import RsvpController from 'server/lib/modules/core/modules/api/controllers/rsvp-controller';

@Module({
    components:  [ LoggerService ],
    exports:     [ LoggerService ],
    modules:     [  ],
    controllers: [ RsvpController ],
})
export class ApiModule implements NestModule {
    public configure(consumer: MiddlewaresConsumer) {
        /* empty */
    }
}
