// tslint:disable:arrow-parens
import { Entity } from 'typeorm/decorator/entity/Entity';
import { Column, JoinColumn, JoinTable, ManyToMany, ManyToOne, OneToOne, PrimaryGeneratedColumn } from 'typeorm';
import { ColumnTypes } from 'typeorm/metadata/types/ColumnTypes';
import Invitation from 'server/lib/modules/core/modules/api/entity/invitation';


@Entity()
export default class Person {
    @PrimaryGeneratedColumn()
    public id?: number;

    @Column(ColumnTypes.STRING, { nullable: true })
    public name: string = null;

    @ManyToOne(type => Invitation, inv => inv.people, {
        cascadeInsert: true,
        cascadeUpdate: true,
    })
    public invitation: Invitation;

    @Column()
    public isAttending: boolean = false;

    @Column(ColumnTypes.STRING, { nullable: true })
    public dietary: string = null;

    @ManyToMany(type => Person, {
        cascadeInsert: true,
        cascadeUpdate: true,
    })
    @JoinTable()
    public related: Person[] = [];
}
