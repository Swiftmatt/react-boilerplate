// tslint:disable:arrow-parens
import { Entity } from 'typeorm/decorator/entity/Entity';
import { Column, JoinColumn, OneToMany, OneToOne, PrimaryColumn, PrimaryGeneratedColumn } from 'typeorm';
import { ColumnTypes } from 'typeorm/metadata/types/ColumnTypes';
import Person from 'server/lib/modules/core/modules/api/entity/person';

import * as Moniker from 'moniker';
import { RelationshipType } from './constants/relationship-type';
const generator = Moniker.generator([ Moniker.adjective, Moniker.adjective, Moniker.noun ], {
    maxSize: 5,
    glue: '-',
});

export function generateCode() {
    return generator.choose().split('-').map((x) => {
        const chars = x.split('');
        return chars.slice(0, 1).join('').toUpperCase() + chars.slice(1).join('');
    }).join('');
}


@Entity()
export default class Invitation {
    @PrimaryColumn()
    public readonly code: string = generateCode();

    @OneToMany(type => Person, person => person.invitation, {
        cascadeInsert: true,
        cascadeUpdate: true,
    })
    public people: Person[] = [];

    @Column()
    public relationshipType: RelationshipType = RelationshipType.Single;

    @Column()
    public adolescentsAttending: number = 0;

    @Column()
    public maxAdolescents: number = 0;

    @Column(ColumnTypes.STRING, { nullable: true })
    public adolescentsDietary: string = null;

    @Column(ColumnTypes.TEXT, { nullable: true })
    public message: string = null;

    @Column(ColumnTypes.DATETIME, { nullable: true })
    public rsvpAt: Date = null;

}
