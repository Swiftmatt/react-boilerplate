import { DefaultController } from 'server/lib/modules/core/controllers/default-controller';
import { Body, Controller, Get, HttpStatus, Param, Post, Response } from '@nestjs/common';
import * as Express from 'express';
import { getConnection } from 'typeorm';
import Person from 'server/lib/modules/core/modules/api/entity/person';
import Invitation from 'server/lib/modules/core/modules/api/entity/invitation';
import * as _ from 'lodash';
import {RelationshipType} from '../entity/constants/relationship-type';

interface IPostRsvpBody {
    invitation: Invitation;
}

@Controller('/wedding/api/rsvp')
export default class RsvpController extends DefaultController {
    @Get('/generate')
    public async generateRsvp(@Response() res: Express.Response, @Body() body: IPostRsvpBody) {

        const connection = await getConnection();
        const personRepo = connection.getRepository(Person);

        // tslint:disable-next-line:no-object-literal-type-assertion
        const person = personRepo.create({
            name: 'Brett Striker',
        } as Person);

        await personRepo.persist(person);

        // tslint:disable-next-line:no-object-literal-type-assertion
        const person2 = personRepo.create({
            name: 'Sally Green',
        } as Person);

        await personRepo.persist(person2);

        person.related.push(person2);

        const invitationRepo = connection.getRepository(Invitation);

        // tslint:disable-next-line:no-object-literal-type-assertion
        const invitation = invitationRepo.create({
            people: [person, ...person.related],
        } as Invitation);

        await personRepo.persist([person, ...person.related]);
        await invitationRepo.persist(invitation);

        res.status(HttpStatus.OK).json({invitation});
    }

    @Get('/:code')
    public async getRsvp(@Response() res: Express.Response, @Param('code') reqCode: string) {
        const connection = await getConnection();
        const invitationRepo = connection.getRepository(Invitation);
        const invitation = await invitationRepo.findOne({ code: reqCode }, {
            alias: 'inv',
            leftJoinAndSelect: {
                people: 'inv.people',
            },
        });

        if (!invitation) {
            return res.status(HttpStatus.NOT_FOUND).json({ error: 'An invalid RSVP Code was provided. Please try again.'});
        }

        res.status(HttpStatus.OK).json({ invitation });
    }

    @Post('/:code')
    public async postRsvp(@Response() res: Express.Response, @Param('code') reqCode: string, @Body() body: IPostRsvpBody) {

        const reqInvitation = body.invitation;

        if (!reqInvitation) {
            return res.status(HttpStatus.BAD_REQUEST).json({ error: 'An RSVP was not provided.', _body: body });
        }

        if (reqInvitation.code !== reqCode) {
            return res.status(HttpStatus.BAD_REQUEST).json({ error: 'The provided RSVP does not have a valid RSVP Code.', _body: body });
        }

        const connection = await getConnection();
        const invitationRepo = connection.getRepository(Invitation);
        const personRepo = connection.getRepository(Person);

        const invitation = await invitationRepo.findOne({ code: reqCode }, {
            alias: 'inv',
            leftJoinAndSelect: {
                people: 'inv.people',
                related: 'people.related',
            },
        });

        if (!invitation) {
            return res.status(HttpStatus.NOT_FOUND).json({ error: 'The provided RSVP did not match an invitation. Please try again.'});
        }
        if (reqInvitation.people.length < 1 || reqInvitation.people.length > 2) {
            return res.status(HttpStatus.BAD_REQUEST).json({ error: 'An invalid number of guests are selected.' });
        }

        if (_.map(invitation.people, (x) => x.id).sort().join('-') !== _.map(reqInvitation.people, (x) => x.id).sort().join('-')) {
            return res.status(HttpStatus.BAD_REQUEST).json({
                error: 'The guests provided for this invitation were not found.',
                _a: _.map(invitation.people, (x) => x.id).sort().join('-'),
                _b: _.map(reqInvitation.people, (x) => x.id).sort().join('-'),
            });
        }

        if (reqInvitation.adolescentsAttending > invitation.maxAdolescents) {
            return res.status(HttpStatus.BAD_REQUEST).json({
                error: 'Invalid number of \'Adolescents Attending\' selected.',
            });
        }

        if (invitation.people[0].name !== reqInvitation.people[0].name) {
            return res.status(HttpStatus.BAD_REQUEST).json({
                error: 'Cannot modify first guest\'s name',
            });
        }

        if (invitation.relationshipType === RelationshipType.Couple && invitation.people[1].name !== reqInvitation.people[1].name) {
            return res.status(HttpStatus.BAD_REQUEST).json({
                error: 'Cannot modify second guest\'s name.',
            });
        }

        reqInvitation.rsvpAt = new Date();

        Object.assign(invitation, reqInvitation);

        // Re-apply the relationships to the primary person
        // if there was more than 2, using a for loop and
        // related.slice();
        // related.splice(indexOfCurrentPerson, 1);
        // would remove the currentPerson, then you could store the remaining people in their relationship
        const [primaryPerson, secondaryPerson] = invitation.people;
        primaryPerson.related = invitation.people.slice(1); // [secondaryPerson]
        // secondaryPerson.related = [primaryPerson];

        await invitationRepo.persist(invitation);
        await personRepo.persist(invitation.people);

        res.status(HttpStatus.OK).json({ invitation });
    }
}
