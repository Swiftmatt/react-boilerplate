import { Controller, UseFilters, MiddlewaresConsumer } from '@nestjs/common';
import { DefaultExceptionFilter } from 'server/lib/modules/core/exception-filters/default-exception-filter';
import { LoggerService } from 'server/lib/modules/core/components/logger-service';


@Controller()
@UseFilters(new DefaultExceptionFilter(new LoggerService()))
export class DefaultController {}
