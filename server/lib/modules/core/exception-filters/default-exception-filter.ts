import * as Express from 'express';
import { Catch, ExceptionFilter, HttpStatus } from '@nestjs/common';
import { Logger, LoggerService } from 'server/lib/modules/core/components/logger-service';
import { isDev } from 'server/lib/utils/env';
import { HttpException } from '@nestjs/core';

interface IResponseObj {
    statusCode: number;
    error: string | object;
}

@Catch(Error)
export class DefaultExceptionFilter implements ExceptionFilter {

    private logger: typeof Logger;

    constructor(private loggerService: LoggerService) {
        this.logger = loggerService.getLogger();
    }

    public catch(exception: Error, response: Express.Response) {
        this.logger.debug('--- DefaultExceptionFilter ---');
        const responseObj: IResponseObj = {
            statusCode: HttpStatus.INTERNAL_SERVER_ERROR,
            error:      exception.message,
        };
        if (exception instanceof HttpException) {
            responseObj.statusCode = exception.getStatus();
            responseObj.error      = exception.getResponse();
            // if dev then print the whole error
            if (isDev) {
                Object.assign(responseObj, exception);
            }
        }
        response.status(responseObj.statusCode).json(responseObj);
        if (responseObj.statusCode === HttpStatus.INTERNAL_SERVER_ERROR || isDev) {
            this.logger.error(exception.toString());
            throw exception;
        }
    }
}
