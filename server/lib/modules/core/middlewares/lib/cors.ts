import * as Express from 'express';

export interface ICorsOptions {
    allowOrigin?: string;
    allowMethods?: string;
    allowHeaders?: string;
}

export function corsHandler(options?: ICorsOptions): Express.RequestHandler {
    return (req: Express.Request, res: Express.Response, next: Express.NextFunction) => {
        applyCorsHeaders(res, options);

        next();
    };
}

export function applyCorsHeaders(res: Express.Response, options: ICorsOptions = {}) {
    res.header('Access-Control-Allow-Origin', options.allowOrigin || '*');
    res.header('Access-Control-Allow-Methods', options.allowMethods || 'GET, OPTIONS');
    res.header('Access-Control-Allow-Headers', options.allowHeaders || 'Origin, X-Requested-With, Content-Type, Accept, Key, Cache-Control');
}

export function allowCrossOriginResourceSharing(req: Express.Request, res: Express.Response, next: Express.NextFunction) {
    applyCorsHeaders(res);

    next();
}

