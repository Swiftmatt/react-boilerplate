import * as Express from 'express';
import { Middleware, NestMiddleware } from '@nestjs/common';
import { Logger, LoggerService } from 'server/lib/modules/core/components/logger-service';

@Middleware()
export class SimpleLoggerMiddleware implements NestMiddleware {

    private logger: typeof Logger;

    constructor(private loggerService: LoggerService) {
        this.logger = loggerService.getLogger();
    }

    public resolve() {
        return (req: Express.Request, res: Express.Response, next: Express.NextFunction) => {
            this.logger.debug(`${req.method}: ${req.originalUrl} -> ${res.statusCode}`);
            next(); // Passing the request to the next handler in the stack.
        };
    }
}
