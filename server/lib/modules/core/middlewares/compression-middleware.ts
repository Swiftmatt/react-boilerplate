import { Middleware, NestMiddleware } from '@nestjs/common';
import * as Compression from 'compression';
import * as Express from 'express';

@Middleware()
export class CompressionMiddleware implements NestMiddleware {

    public resolve(options: Compression.CompressionOptions = { threshold: 0, filter: () => true }): Express.RequestHandler {
        return Compression(options);
    }
}
