import { Middleware, NestMiddleware } from '@nestjs/common';
import { Logger, LoggerService } from 'server/lib/modules/core/components/logger-service';
import { corsHandler, ICorsOptions } from 'server/lib/modules/core/middlewares/lib/cors';
import * as Express from 'express';

@Middleware()
export class CorsMiddleware implements NestMiddleware {

    constructor(private loggerService: LoggerService) { }

    public resolve(options: ICorsOptions = {}): Express.RequestHandler {
        return corsHandler(options);
    }
}
