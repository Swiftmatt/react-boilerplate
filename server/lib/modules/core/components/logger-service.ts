import { Component } from '@nestjs/common';
import { Logger } from 'server/lib/logger';
export * from 'server/lib/logger';

@Component()
export class LoggerService {
    private logger = Logger;

    public getLogger(): typeof Logger {
        return this.logger;
    }
}
