import { MiddlewaresConsumer, Module, NestModule, RequestMethod } from '@nestjs/common';
import { SimpleLoggerMiddleware } from 'server/lib/modules/core/middlewares/simple-logger-middleware';
import { LoggerService } from 'server/lib/modules/core/components/logger-service';
import { CorsMiddleware } from 'server/lib/modules/core/middlewares/cors-middleware';
import { CompressionMiddleware } from 'server/lib/modules/core/middlewares/compression-middleware';
import { ApiModule } from 'server/lib/modules/core/modules/api';

@Module({
    components:  [ LoggerService ],
    exports:     [ LoggerService ],
    modules:     [ ApiModule ],
})
export class CoreModule implements NestModule {
    public configure(consumer: MiddlewaresConsumer) {
        consumer.apply([
            SimpleLoggerMiddleware,
            CorsMiddleware,
            CompressionMiddleware,
        ]).forRoutes({
            path: '*', method: RequestMethod.ALL,
        });
    }
}
