/**
 *  From: https://github.com/Microsoft/TypeScript/issues/3192
 *
 *  Example:
 *
 *  type  Color  = EnumType<typeof Color>;
 *  var   Color  = makeStringEnum('Red', 'Green');
 */
export type EnumType<T> = T[keyof T];

export function makeEnumFromList<X extends string>(...x: X[]): {[K in X]: K } {
    const o: any = {};
    for (const k of x) {
        o[ k ] = k;
    }
    return o;
}

export function makeEnumFromObject<X>(x: X): {[K in keyof X]: X[K] } {
    return x;
}
