import * as Yargs from 'yargs';

export interface IArgs {
    verbose: number;
    port: number;
}

enum LogLevel {
    Error = 0,
    Warn = 1,
    Info = 2,
    Verbose = 3,
    Debug = 4,
    Silly = 5,
}

export const Argv: Yargs.Argv & IArgs = Yargs
    .usage('Usage: $0 [options]')
    .count('verbose')
    .alias('v', 'verbose')
    .argv;

export const logLevel = (LogLevel[Argv.verbose] || LogLevel[LogLevel.Silly]).toLowerCase();

export default Argv;
