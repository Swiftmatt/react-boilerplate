export const NODE_ENV = process.env.NODE_ENV || 'development';
export const isDev    = NODE_ENV === 'development';
export default isDev;
