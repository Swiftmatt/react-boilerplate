import * as Winston from 'winston';
import * as _ from 'lodash';
export import Logger = Winston;

export class LoggerUtils {
    public static getDefaultOptions(options: Logger.LoggerOptions = {}) {
        return _.defaults<Logger.LoggerOptions, Logger.LoggerOptions, Logger.LoggerOptions>(
            {},
            options,
            {
                level: 'warn',
                exitOnError: true,
                handleExceptions: true,
                humanReadableUnhandledException: true,
                transports: [
                    new (Logger.transports.Console)(),
                ],
            },
        );
    }

    public static createScopedInstance(options?: Logger.LoggerOptions) {
        return new Logger.Logger(options);
    }

    public static getGlobalInstance(): typeof Logger {
        return Logger;
    }
}
