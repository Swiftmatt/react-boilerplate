import * as Winston from 'winston';
import { LoggerUtils } from 'server/lib/logger/utils';
export * from 'server/lib/logger/utils';

export class LoggerFactory {
    public static createScopedInstance(options: Winston.LoggerOptions = {
        transports: [
            new (Winston.transports.Console)({
                timestamp: true,
                colorize:  true,
            }),
        ],
    }) {
        return LoggerUtils.createScopedInstance(LoggerUtils.getDefaultOptions(options));
    }
}

export default LoggerFactory;
