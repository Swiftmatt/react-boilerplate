import { LoggerFactory } from 'server/lib/logger/factory';
import { logLevel } from 'server/lib/utils/argv';
import { Logger } from 'server/lib/logger/utils';
export * from 'server/lib/logger/utils';

export const defaultLogger = LoggerFactory.createScopedInstance({
    level: logLevel,
    transports: [
        new (Logger.transports.Console)({
            timestamp: true,
            colorize: true,
        }),
    ],
});
