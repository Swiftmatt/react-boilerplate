import * as React from 'react';
import { Link } from 'react-router';

import NavLink from './NavLink';
import Dropdown from './Dropdown';

import routes from './routes';

const styles = require('./NavBar.scss');

const navLinks = routes.map((route, i) => {
  if (route.disabled === true) {
    return null;
  }

  if ( !('dropdown' in route) ) {
    return <li key={i}><NavLink route={route} key={route.link} /></li>;
  }

  return (
    <li className="dropdown" key={i}>
      <NavLink route={route} key={route.link} />
      <Dropdown routes={route.dropdown} />
    </li>
  );
});

class NavBar extends React.Component<{}, {}> {
  public render() {
    return (
      <div id="NavBar">
        <ul>{navLinks}</ul>
        <button>
          <span />
          <span />
          <span />
        </button>
      </div>
    );
  }
}

export default NavBar;
