interface IRoute {
    text: string;
    link: string;
    type: 'internal' | 'external';
    dropdown?: IRoute[];
    disabled?: boolean;
    priority?: boolean;
}

const routes: IRoute[] = [
  {
    text    : 'MM + AB',
    link    : '/wedding/',
    type    : 'internal',
    // dropdown: [
    //   {
    //     text: 'Our Story',
    //     link: '/mm-ab/our-story',
    //     type: 'internal',
    //   },
    //   {
    //     text: 'Our Engagement',
    //     link: '/mm-ab/our-engagement',
    //     type: 'internal',
    //   },
    // ],
  },
  // {
  //   text    : 'VIPs',
  //   link    : '/',
  //   type    : 'internal',
  //   dropdown: [
  //     {
  //       text: 'Family',
  //       link: '/vips/family',
  //       type: 'internal',
  //     },
  //     {
  //       text: 'Wedding Party',
  //       link: '/vips/wedding-party',
  //       type: 'internal',
  //     },
  //     {
  //       text: 'Wedding Team',
  //       link: '/vips/wedding-team',
  //       type: 'internal',
  //     },
  //   ],
  // },
  // {
  //   text    : 'Details',
  //   link    : '/',
  //   type    : 'internal',
  //   dropdown: [
  //     {
  //       text: 'Travel',
  //       link: '/details/travel',
  //       type: 'internal',
  //     },
  //     {
  //       text: 'Ceremony',
  //       link: '/details/ceremony',
  //       type: 'internal',
  //     },
  //     {
  //       text: 'Reception',
  //       link: '/details/reception',
  //       type: 'internal',
  //     },
  //     {
  //       text: 'Accommodations',
  //       link: '/details/accommodations',
  //       type: 'internal',
  //     },
  //   ],
  // },
  // {
  //   text    : 'Photos',
  //   link    : '/',
  //   type    : 'internal',
  //   dropdown: [
  //     {
  //       text: 'Wedding',
  //       link: '/photos/wedding',
  //       type: 'internal',
  //     },
  //     {
  //       text: 'Engagement',
  //       link: '/photos/engagement',
  //       type: 'internal',
  //     },
  //   ],
  // },
  // {
  //   text    : 'Instagram',
  //   link    : '/',
  //   type    : 'internal',
  //   disabled: true,
  // },
  {
    text    : 'RSVP',
    link    : '/wedding/rsvp',
    type    : 'internal',
    priority: true,
  },
];

export default routes;
