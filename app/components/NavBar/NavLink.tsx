import * as React from 'react';
import { Link } from 'react-router';

function NavLink(props) {

  const route = props.route;
  let className = '';

  if ('priority' in route && route.priority) {
    className = 'priority-nav';
  }

  return (
    <Link to={route.link} className={className}>{route.text}</Link>
  );
}

export default NavLink;
