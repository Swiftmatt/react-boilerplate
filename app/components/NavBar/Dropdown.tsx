import * as React from 'react';
import { Link } from 'react-router';

import NavLink from './NavLink';

function Dropdown(props) {

  const routes = props.routes;

  return (
    <div>
      {
        routes.map((route) => {
          if (route.disabled === true) {
            return null;
          }

          return <NavLink route={route} key={route.link} />;
        })
      }

      {/*
        <li><a href="#home">Home</a></li>
        <li><a href="#news">News</a></li>
        <li className="dropdown">
          <a href="#">Dropdown</a>
          <div>
            <a href="#">Link 1</a>
            <a href="#">Link 2</a>
            <a href="#">Link 3</a>
          </div>
        </li>
      */}
    </div>
  );
}

export default Dropdown;
