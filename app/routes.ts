// These are the pages you can go to.
// They are all wrapped in the App component, which should contain the navbar etc
// See http://blog.mxstbr.com/2016/01/react-apps-with-pages for more information
// about the code splitting business
import { getAsyncInjectors } from './utils/asyncInjectors';
import { PlainRoute } from 'react-router';

type RoutesCallback = (err: any, route: PlainRoute) => void;

const errorLoading = (cb: RoutesCallback) => (err: Error) => {
    console.error('Dynamic page loading failed', err);
    cb(err, null);
};

const loadModule = (cb: RoutesCallback) => (componentModule) => {
    cb(null, componentModule.default);
};

export interface IExtendedRouteProps extends PlainRoute {
    name?: string;
}

async function notFound(cb: RoutesCallback) {
    System.import('app/containers/NotFoundPage')
        .then(loadModule(cb))
        .catch(errorLoading(cb));
}

export default function createRoutes(store): IExtendedRouteProps[] {
    // create reusable async injectors using getAsyncInjectors factory
    const { injectReducer, injectSagas } = getAsyncInjectors(store);

    return [
        {
            path: '/wedding/',
            name: 'root',
            getComponent(nextState, cb) {
                /*

                 Example:

                 const importModules = Promise.all([
                 System.import('app/containers/RootPage/reducer'),
                 System.import('app/containers/RootPage/sagas'),
                 System.import('app/containers/RootPage'),
                 ]);

                 const renderRoute = loadModule(cb);

                 importModules.then(([reducer, sagas, component]) => {
                 injectReducer('root', reducer.default);
                 injectSagas(sagas.default);

                 renderRoute(component);
                 });

                 importModules.catch(errorLoading);
                 */

                System.import('app/containers/RootPage')
                    .then(loadModule(cb))
                    .catch(errorLoading(cb));
            },
        }, {
            path: '/wedding/mm-ab/our-story',
            name: 'ourstory',
            getComponent(nextState, cb) {
                System.import('containers/MmAb/OurStoryPage')
                    .then(loadModule(cb))
                    .catch(errorLoading);
            },
        }, {
            path: '/wedding/mm-ab/our-engagement',
            name: 'ourengagement',
            getComponent(nextState, cb) {
                System.import('containers/MmAb/OurEngagementPage')
                    .then(loadModule(cb))
                    .catch(errorLoading);
            },


        }, {
            path: '/wedding/vips/family',
            name: 'family',
            getComponent(nextState, cb) {
                System.import('containers/Vips/FamilyPage')
                    .then(loadModule(cb))
                    .catch(errorLoading);
            },
        }, {
            path: '/wedding/vips/wedding-party',
            name: 'weddingparty',
            getComponent(nextState, cb) {
                System.import('containers/Vips/WeddingPartyPage')
                    .then(loadModule(cb))
                    .catch(errorLoading);
            },
        }, {
            path: '/wedding/vips/wedding-team',
            name: 'weddingteam',
            getComponent(nextState, cb) {
                System.import('containers/Vips/WeddingTeamPage')
                    .then(loadModule(cb))
                    .catch(errorLoading);
            },


        }, {
            path: '/wedding/details/travel',
            name: 'travel',
            getComponent(nextState, cb) {
                System.import('containers/Details/TravelPage')
                    .then(loadModule(cb))
                    .catch(errorLoading);
            },
        }, {
            path: '/wedding/details/ceremony',
            name: 'ceremony',
            getComponent(nextState, cb) {
                System.import('containers/Details/CeremonyPage')
                    .then(loadModule(cb))
                    .catch(errorLoading);
            },
        }, {
            path: '/wedding/details/reception',
            name: 'reception',
            getComponent(nextState, cb) {
                System.import('containers/Details/ReceptionPage')
                    .then(loadModule(cb))
                    .catch(errorLoading);
            },
        }, {
            path: '/wedding/details/accommodations',
            name: 'accommodations',
            getComponent(nextState, cb) {
                System.import('containers/Details/AccommodationsPage')
                    .then(loadModule(cb))
                    .catch(errorLoading);
            },


        }, {
            path: '/wedding/photos/wedding',
            name: '',
            getComponent(nextState, cb) {
                System.import('containers/Photos/WeddingPhotosPage')
                    .then(loadModule(cb))
                    .catch(errorLoading);
            },
        }, {
            path: '/wedding/photos/engagement',
            name: '',
            getComponent(nextState, cb) {
                System.import('containers/Photos/EngagementPhotosPage')
                    .then(loadModule(cb))
                    .catch(errorLoading);
            },


        }, {
            path: '/wedding/rsvp',
            name: 'rsvp',
            getComponent(nextState, cb) {
                System.import('app/containers/RsvpPage')
                    .then(loadModule(cb))
                    .catch(errorLoading(cb));
            },
        }, {
            path: '/wedding/rsvp/:code',
            name: 'rsvp_code',
            getComponent(nextState, cb) {
                System.import('app/containers/RsvpPage')
                    .then(loadModule(cb))
                    .catch(errorLoading(cb));
            },
        }, {
            path: '*',
            name: 'notfound',
            getComponent(nextState, cb) {
                notFound(cb);
            },
        },
    ];
}
