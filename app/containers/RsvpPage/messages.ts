/*
 * RsvpPage Messages
 *
 * This contains all the text for the RsvpPage component.
 */
import { defineMessages } from 'react-intl';

export default defineMessages({
    header: {
        id: 'boilerplate.containers.RsvpPage.header',
        defaultMessage: 'RSVP',
    },
});
