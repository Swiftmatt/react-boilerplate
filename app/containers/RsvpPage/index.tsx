/**
 * RsvpPage
 */

// tslint:disable:jsx-no-lambda

import * as React from 'react';
import { connect } from 'react-redux';
import { push, RouterAction } from 'react-router-redux';

import messages from './messages';
import { FormattedMessage } from 'react-intl';
import { Helmet } from 'react-helmet';

import './styles.scss';

type ToastAction = string | ({
    onClick?: () => void;
    label: string
});

interface IToast {
    text: string;
    action?: ToastAction;
}

interface IProps extends RouteComponentProps<{ code?: string }, {}> {
    dispatch?: (action: RouterAction) => void;
}

interface IState {
    toasts?: IToast[];
    invitation?: Invitation;
}

import {
    Card,
    CardTitle,
    CardActions,
    CardText,
    Button,
    TextField,
    Snackbar,
    Switch as OriginalSwitch,
    SwitchProps,
} from 'react-md';
import SelectField from 'react-md/lib/SelectFields';
import { RouteComponentProps } from 'react-router';
import Invitation from 'server/lib/modules/core/modules/api/entity/invitation';
import { RelationshipType } from '../../../server/lib/modules/core/modules/api/entity/constants/relationship-type';

function Switch(props: SwitchProps & { label?: string }) {
    return ( <OriginalSwitch {...props} /> );
}

export class RsvpPage extends React.Component<IProps, IState> {

    constructor(props) {
        super(props);
        this._redirect = this._redirect.bind(this);
        this._addToast = this._addToast.bind(this);
        this._removeToast = this._removeToast.bind(this);
        this._handleSave = this._handleSave.bind(this);

        this.state = {
            toasts: [],
        };
    }

    private async _handleSave() {
        try {
            const response = await fetch(`/wedding/api/rsvp/${this.props.params.code}`, {
                headers: { 'Content-Type': 'application/json' },
                method: 'POST',
                body: JSON.stringify({ invitation: this.state.invitation }),
            });
            if (!response.ok) {
                const body = await response.json();
                throw new Error(body.error);
            }
            const result = await response.json() as { invitation: Invitation };
            this.setState({ invitation: result.invitation });
            this._addToast('Your RSVP was sent successfully!', 'Ok');
        } catch (err) {
            this._addToast(err instanceof Error ? err.message : err, 'Ok');
        }
    }

    private _addToast(text: string, action?: ToastAction) {
        const toasts = this.state.toasts.slice();
        toasts.push({ text, action });

        this.setState({ toasts });
    }

    private _removeToast() {
        const [ , ...toasts ] = this.state.toasts;
        this.setState({ toasts });
    }

    private _redirect() {
        this.props.dispatch(push('/'));
    }

    public async componentWillReceiveProps(nextProps: IProps) {
        if (!nextProps.params.code || this.props.params.code === nextProps.params.code) {
            this.setState({ invitation: undefined });
            return;
        }
        await this.fetchInvitation(nextProps.params.code);
    }

    public async componentDidMount() {
        if (!this.props.params.code) {
            return;
        }
        await this.fetchInvitation(this.props.params.code);
    }

    private async fetchInvitation(code: string) {
        if (!code) {
            this._addToast('Please enter an RSVP code!', 'Ok');
            return;
        }

        try {
            const response = await fetch(`/wedding/api/rsvp/${code}`);
            if (!response.ok) {
                const body = await response.json();
                throw new Error(body.error);
            }
            const result = await response.json() as { invitation: Invitation };
            this.setState({ invitation: result.invitation });
        } catch (err) {
            this._addToast(err instanceof Error ? err.message : err, 'Ok');
        }
    }

    public render() {
        const invitation = this.state.invitation;
        const cardSubtitle = (!invitation) ? 'Please enter your RSVP Code below. (RSVP Codes are case-sensitive)' : null;

        const people = invitation ? invitation.people.map((person, index) => {
            const isPlusOne: boolean = Boolean(invitation.relationshipType === RelationshipType.Single && index);
            const isPrimary: boolean = !index;
            const isSecondary: boolean = !isPlusOne && !isPrimary;

            return (
                <section key={index} flex="grow">
                    <hr />
                    <section layout="row" style={ {marginRight: '0.5rem', marginLeft: '0.5rem'} }>
                        <section layout="column" layout-align="center center" className="isAttending">
                            <Switch
                                id={`person_${index}_attending`}
                                name={`${index}_attending`}
                                checked={invitation.people[ index ].isAttending}
                                onChange={(value) => {
                                    invitation.people[ index ].isAttending = value;
                                    this.setState({ invitation: { ...this.state.invitation } });
                                }}
                                labelBefore={true}
                                aria-label={`person ${index + 1} attending toggle switch`}
                                label={
                                    (invitation.people[ index ].isAttending) ? 'Attending' : 'Not Attending'
                                }
                                style={{
                                    flexDirection: 'column',
                                    justifyContent: 'center',
                                    alignItems: 'center',
                                    alignContent: 'center',
                                    width: '100px',
                                }}
                            />
                        </section>
                        <section className="person" flex="grow">
                            <TextField
                                id={`person_${index}_name`}
                                className="md-full-width"
                                label="Guest Name"
                                value={invitation.people[ index ].name}
                                disabled={isPrimary || isSecondary}
                                fullWidth={true}
                                onChange={(value) => {
                                    invitation.people[ index ].name = value.toString();
                                    this.setState({ invitation: { ...this.state.invitation } });
                                }}
                            />
                            <TextField
                                id={`person_${index}_dietary`}
                                className="md-full-width"
                                label="Dietary Concerns"
                                value={invitation.people[ index ].dietary}
                                fullWidth={true}
                                onChange={(value) => {
                                    invitation.people[ index ].dietary = value.toString();
                                    this.setState({ invitation: { ...this.state.invitation } });
                                }}
                                maxLength={255}
                            />
                        </section>
                    </section>
                </section>

            );
        }) : [];

        return (
            <article>
                <Helmet>
                    <title>RSVP</title>
                </Helmet>
                <Card>
                    <CardTitle title={<FormattedMessage {...messages.header} />} subtitle={cardSubtitle} />
                    <CardText>
                        <form>
                            {
                                invitation ? (
                                    <section>
                                        <section layout="column" layout-gt-md="row">
                                            {people}
                                        </section>

                                        <hr />
                                        {invitation.maxAdolescents > 0 ? (
                                            <section layout="column" layout-gt-md="row">
                                                <section layout="row" flex="grow">
                                                    <SelectField
                                                        id="adolescents"
                                                        className="md-full-width"
                                                        aria-label="Adolescents attending selection box"
                                                        label="Adolescents Attending"
                                                        disabled={invitation.maxAdolescents === 0}
                                                        defaultValue={0}
                                                        value={invitation.adolescentsAttending}
                                                        style={{
                                                            marginRight: '0.5rem',
                                                            marginLeft: '0.5rem',
                                                        }}
                                                        menuItems={_.range(0, invitation.maxAdolescents
                                                            ? invitation.maxAdolescents + 1
                                                            : invitation.maxAdolescents)}
                                                        onChange={(value) =>
                                                            this.setState({
                                                                invitation: {
                                                                    ...this.state.invitation,
                                                                    adolescentsAttending: parseInt(value.toString(), 10),
                                                                },
                                                            })
                                                        }
                                                    />
                                                </section>
                                                <section layout="row" flex="grow">
                                                    <TextField
                                                        id={`adolescents_dietary`}
                                                        label="Adolescent Dietary Concerns"
                                                        value={invitation.adolescentsDietary}
                                                        disabled={invitation.adolescentsAttending === 0}
                                                        rows={1}
                                                        maxRows={4}
                                                        maxLength={255}
                                                        style={{
                                                            marginRight: '0.5rem',
                                                            marginLeft: '0.5rem',
                                                        }}
                                                        onChange={(value) =>
                                                            this.setState({
                                                                invitation: {
                                                                    ...this.state.invitation,
                                                                    adolescentsDietary: value.toString(),
                                                                },
                                                            })
                                                        }
                                                    />
                                                </section>
                                            </section>
                                        ) : null}
                                        <hr />
                                        <section layout="column" layout-gt-md="row">
                                            <section layout="row" flex="grow">
                                                <TextField
                                                    id={`message`}
                                                    label="Special Message for the Bride and Groom"
                                                    value={invitation.message}
                                                    rows={1}
                                                    maxRows={6}
                                                    onChange={(value) => this.setState({
                                                        invitation: {
                                                            ...this.state.invitation,
                                                            message: value.toString(),
                                                        },
                                                    })
                                                    } />
                                            </section>
                                        </section>
                                    </section>
                                ) : (
                                    <section layout="column">
                                        <section layout="column">
                                            <TextField
                                                id={`code`}
                                                label="RSVP Code"
                                                onKeyDown={
                                                    (event: React.KeyboardEvent<HTMLInputElement>) => {
                                                        if (event.keyCode === 13) {
                                                            this.props.dispatch(push(`/wedding/rsvp/${event.currentTarget.value}`));
                                                        }
                                                    }
                                                }
                                            />
                                        </section>
                                        <section layout="row" layout-align="end start">
                                            <Button
                                                label="GO"
                                                primary
                                                raised
                                                iconBefore={false}
                                                onClick={() => {
                                                    const code = (document.querySelector('#code') as HTMLInputElement).value;
                                                    this.props.dispatch(push(`/wedding/rsvp/${code}`));
                                                    this.fetchInvitation(code);
                                                }}
                                            >favorite</Button>
                                        </section>
                                    </section>
                                )
                            }
                        </form>
                    </CardText>
                    {invitation ? (
                        <CardActions layout="row" layout-align="space-between center">
                            <section layout="column">
                                {
                                    (invitation && invitation.rsvpAt)
                                        ? (
                                            <div>
                                                <p style={{textDecoration: 'underline'}}>Last RSVP Sent on: </p>
                                                <p>
                                                    {(new Date(invitation.rsvpAt)).toLocaleDateString('en-US', {
                                                        month: 'long',
                                                        day: 'numeric',
                                                        year: 'numeric',
                                                    })}
                                                </p>
                                                <p>
                                                    {(new Date(invitation.rsvpAt)).toLocaleString('en-US', {
                                                        hour: 'numeric',
                                                        minute: 'numeric',
                                                        second: 'numeric',
                                                    })}
                                                </p>
                                            </div>
                                        )
                                        : null
                                }
                            </section>
                            <section layout="row" layout-align="end start">
                                <Button label="Send RSVP" primary raised iconBefore={false} onClick={this._handleSave}>send</Button>
                            </section>
                        </CardActions>
                    ) : null}
                </Card>
                <Snackbar toasts={this.state.toasts} onDismiss={this._removeToast} />
            </article>

        );
    }
}

// Wrap the component to inject dispatch and state into it
export default connect()(RsvpPage);
