/**
 * RootPage
 *
 * This is the page we show when the user visits a url that doesn't have a route
 */

import * as React from 'react';
import { connect } from 'react-redux';
import { push, RouterAction } from 'react-router-redux';

import messages from './messages';
import { FormattedMessage } from 'react-intl';
import { Helmet } from 'react-helmet';

const ourPicture = require('./mmab.jpg');


interface IProps {
    dispatch?: (action: RouterAction) => void;
}

import { Card, CardTitle, CardText, CardActions, Button, Media } from 'react-md';

export class RootPage extends React.Component<IProps, {}> {

    constructor(props) {
        super(props);
        this.redirect = this.redirect.bind(this);
    }

    private redirect() {
        this.props.dispatch(push('/'));
    }

    public render() {
        return (
            <article>
                <Helmet>
                    <title>MM + AB</title>
                </Helmet>
                <Card>
                    <CardTitle title={ <FormattedMessage {...messages.header} /> } subtitle={'August 12th, 2017'} />
                    <CardText>
                        <Media aspectRatio="4-3">
                            <img src={ourPicture} style={{height: 'auto'}} />
                        </Media>
                    </CardText>
                </Card>
            </article>
        );
    }
}

// Wrap the component to inject dispatch and state into it
export default connect()(RootPage);
