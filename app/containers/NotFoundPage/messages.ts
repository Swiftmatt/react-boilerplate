/*
 * NotFoundPage Messages
 *
 * This contains all the text for the NotFoundPage component.
 */
import { defineMessages } from 'react-intl';

export default defineMessages({
    header: {
        id: 'boilerplate.containers.NotFoundPage.header',
        defaultMessage: 'Page not found.',
    },
    rootButton: {
        id: 'boilerplate.containers.NotFoundPage.root',
        defaultMessage: 'Home',
    },
});
